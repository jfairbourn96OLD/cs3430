#!usr/bin/python

import sys

with open('LINES.txt', 'w') as outfile:
    outfile.write('Long titles only :rage:\n')
    for title in [line for line in sys.stdin.readlines() if len(line) > 15]:
        outfile.write(title)
