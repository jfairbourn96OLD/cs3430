def loop_gen():
    for i in range(10):
        yield i

g = loop_gen()
for i in range(11):
    print g.next()
