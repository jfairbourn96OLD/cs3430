#!/usr/bin/python

import sys

lines = []
with open('lines.txt', 'r') as infile:
    lines = [line for line in infile]
    
with open('LINES.txt', 'w') as outfile:
    for line in lines:
        outfile.write(line.upper())
