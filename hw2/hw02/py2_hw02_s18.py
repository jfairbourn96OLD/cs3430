#/usr/bin/python

#########################################
## CS 3430: S2018: HW02: DNA String Analysis
## Justin Fairbourn
## A01842387
#########################################

from dna_data import *

import re

pat00 = r'(C{2})'
pat01 = r'(T{3,5})'
pat02 = r'(T{3,5}[A|G])' 
pat03 = r'([A|G](?:T{3,10}|C{2,5})G?)' 
pat04 = r'([A|T](?:G{5,15}|C{2,11})T*(?:A{2}|G{3}))'

pat05 = r'A{4,}'
pat06 = r'G{4,}'
pat07 = r'C{4,}'
pat08 = r'T{4,}'

pat09 = r'G{2,5}A+CG{2,5}'
pat10 = r'C{2,5}A+TC{2,5}'
pat11 = r'T{2,5}A+GT{2,5}' 

def find_pat_matches(pat, txt):
    #Create an iterable using find.iter, and from that create a list of 2-tuples where
    #the first element is the matching string, and the second element is its span
    return [(match.group(), match.span()) for match in re.finditer(pat, txt)]

def sort_pat_matches_by_span(pat, txt, reverse=True):
    #Initially I tried to do this all in one line like follows:
    #  return find_pat_matches(pat, txt).sort(key=lambda x: x[1][1]-x[1][0])
    #but this always returned a None object. Why is this?
    #Isn't the above statement the same thing as doing each method call separately, as I've done below?
    #First call find_pat_matches(pat,txt) to get the list we want to sort
    lst = find_pat_matches(pat, txt)

    #Then we sort the list according to its length: x[1][1]-x[1][0]
    lst.sort(key=lambda x: x[1][1]-x[1][0], reverse=True)

    #Then we return it
    return lst

def top_n_pat_matches_by_span(pat, txt, n): 
    #Simple solution that uses list comprehension to get the first n elements returned from
    #  the sort_pat_matches_by_span function
    sorted_matches = sort_pat_matches_by_span(pat, txt, reverse=True)
    if len(sorted_matches) == 0: return
    else: return [sorted_matches[i] for i in xrange(0,n) if i < len(sorted_matches)]
    #return [sort_pat_matches_by_span(pat, txt, reverse=True)[i] for i in xrange(0,n)]

def find_dnas_with_longest_span_match_for_pattern(pat, dna_list):
    #First get the list of longest matching strings in each of the 100 DNA_LIST's, with its name
    lst = [(dna_list_i[0],top_n_pat_matches_by_span(pat, dna_list_i[1], 1)) for dna_list_i in dna_list]   
    
    #We need to unwrap lst to make it easier to zip it up with the names of the DNA_LISTs
    # This should also eliminate any NoneType objects
    lst = [(dna_list[i][0],lst[i][1][0][0],lst[i][1][0][1]) for i in xrange(0,len(lst)) if lst[i][1] is not None]

    #Sort it according to its length (longest first)
    lst.sort(key = lambda x: x[2][1]-x[2][0], reverse=True)
    
    #Return the first element of the resulting sorted list
    return lst[0]

def count_pattern_matches(pat, dna_list):
    #initialize the summing variable, then add the length of the matches list for each DNA_LIST
    match_sum = 0
    for i in xrange(0,len(dna_list)):
        match_sum = match_sum + len(find_pat_matches(pat, dna_list[i][1]))
    return match_sum

