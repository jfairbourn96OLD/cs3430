#!/usr/bin/python3
## print odd integers from STDIN
## to test:
## > more numbers.txt | python3 filter_stdin_odds.py
## bugs to vladimir dot kulyukin at usu dot edu
import sys
for n in [int(x) for x in sys.stdin.readlines() if int(x) % 2 == 0]:
    print(n)


    

