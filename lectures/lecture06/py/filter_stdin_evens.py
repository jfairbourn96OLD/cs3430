#!/usr/bin/python
## print odd integers from STDIN
## to test:
## > more numbers.txt | python filter_stdin_odds.py
## bugs to vladimir dot kulyukin at usu dot edu
from __future__ import print_function
import sys
for n in [int(x) for x in sys.stdin.readlines() if int(x) % 2 == 0]:
    print(n)


    

