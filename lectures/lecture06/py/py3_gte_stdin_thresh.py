#!/usr/bin/python3
## print integers from STDIN that are >= sys.argv[1]
## bugs to vladimir dot kulyukin at usu dot edu
import sys
thresh = int(sys.argv[1])
for n in [int(x) for x in sys.stdin.readlines() if int(x) >= thresh]:
    print(n)

    
