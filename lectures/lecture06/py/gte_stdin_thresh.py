#!/usr/bin/python
## print integers from STDIN that are >= sys.argv[1]
## bugs to vladimir dot kulyukin at usu dot edu
from __future__ import print_function
import sys
thresh = int(sys.argv[1])
for n in [int(x) for x in sys.stdin.readlines() if int(x) >= thresh]:
    print(n)

    
