#!/usr/bin/python

############################
# module: py2_list_comp.py
# author: vladimir kulyukin
# description: list comprehension examples
############################

from __future__ import print_function

def build_list_1a():
    rslt = []
    x = 0
    while x < 11:
        if x % 2 == 0:
            rslt.append(2*x)
        x += 1
    return rslt

def build_list_1b():
    return [2*x for x in xrange(11) if x % 2 == 0]

def problem_1():
    print('ListComp: Problem 1')
    print(build_list_1a())
    print(build_list_1b())

def build_list_2a():
    rslt = []
    x = 0
    while x**2 < 100:
        rslt.append(4*x)
        x += 1
    return rslt

def build_list_2b():
    return [4*x for x in xrange(101) if x**2 < 100]

def problem_2():
    print('ListComp: Problem 2')
    print(build_list_2a())
    print(build_list_2b())

def build_list_3a():
    rslt = []
    x = 0
    while x < 11:
        if x % 2 != 0:
            rslt.append(x**3)
        x += 1
    return rslt

def build_list_3b():
    return [x**3 for x in xrange(11) if x % 2 != 0]

def problem_3():
    print('ListComp: Problem 3')
    print(build_list_3a())
    print(build_list_3b())

def build_list_4a():
    rslt = []
    for x in xrange(6):
        if x % 2 == 0:
            for y in xrange(6):
                if not y % 2 == 0:
                    rslt.append((x,y))
    return rslt

def build_list_4b():
    return [(x,y)
            for x in xrange(6) if x % 2 == 0
            for y in xrange(6) if not y % 2 == 0]

def problem_4():
    print('ListComp: Problem 4')
    print(build_list_4a())
    print(build_list_4b())

if __name__ == '__main__':
    problem_1()
    problem_2()
    problem_3()
    problem_4()
