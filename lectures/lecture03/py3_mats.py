#!/usr/bin/python3

##########################
# module: py3_mats.py
# matrix manip with list comprehension
# vladmir kulyukin
##########################

mat = \
       [
           [1, 1, 1],
           [2, 2, 2],
           [3, 3, 3]
        ]

toop_mat = \
             (
                 (1, 1, 1),
                 (2, 2, 2),
                 (3, 3, 3),
                 )

mat2 = \
        [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8]
        ]

toop_mat2 = \
              (
                  (0, 1, 2),
                  (3, 4, 5),
                  (6, 7, 8)
              )


def mat_loop_sum(mat):
    sum_total = 0
    for r in mat:
        sum_total += sum(r)
    return sum_total

print('list matrix sum w/ loops =', mat_loop_sum(mat))
print('toop matrix sum w/ loops = ', mat_loop_sum(toop_mat))
print()

def mat_listcomp_sum(mat): return sum([sum(r) for r in mat])

print('list matrix sum w/ listcomp = ', mat_listcomp_sum(mat))
print('toop matrix sum w/ listcomp = ', mat_listcomp_sum(mat))
print()

def display_mat(mat):
    for row in mat:
        print(row)
    print()

def mat_loop_col_sum(mat, col_num):
    col_sum_total = 0
    for r in mat:
        col_sum_total += r[col_num]
    return col_sum_total

def test_mat_loop_col_sum(mat, num_cols):
    print('list matrix column sums w/ loops for matrix:')
    display_mat(mat)
    for col_num in range(num_cols):
        print('sum of column', str(col_num), '=', mat_loop_col_sum(mat, col_num))
    print()

def mat_listcomp_col_sum(mat, col_num):
    return sum([row[col_num] for row in mat])

def test_mat_listcomp_col_sum(mat, num_cols):
    print('list matrix column sums w/ list comprehension for matrix:')
    display_mat(mat)
    for col_num in range(num_cols):
        print('sum of column', str(col_num), '=', mat_listcomp_col_sum(mat, col_num))
    print()

## computing column sums of mat
test_mat_loop_col_sum(mat, 3)
test_mat_listcomp_col_sum(mat, 3)

## computing column sums of toop_mat
test_mat_loop_col_sum(toop_mat, 3)
test_mat_listcomp_col_sum(toop_mat, 3)

## computing column sums of mat2
test_mat_loop_col_sum(mat2, 3)
test_mat_listcomp_col_sum(mat2, 3)

## computing column sums of toop_mat2
test_mat_loop_col_sum(toop_mat2, 3)
test_mat_listcomp_col_sum(toop_mat2, 3)


    

 

    
