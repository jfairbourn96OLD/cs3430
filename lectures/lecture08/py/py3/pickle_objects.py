#!/usr/bin/python3

#########################################
# module: pickle_trial.py
# author: vladimir kulyukin
# description: pickling objects in Py3
# to run:
# $python pickle_objects.py dump.pck
#########################################

import pickle
import sys
from Date import Date

date_list = [Date(m=1, d=1, y=2018),
                  Date(m=2, d=1, y=2018),
                  Date(m=2, d=10, y=2018)]

with open(sys.argv[1], 'wb') as outfile:
    pickle.dump(date_list, outfile)
    sys.stdout.write('Dumped dates\n')

with open(sys.argv[1], 'rb') as infile:
    loaded_dates = None
    loaded_dates = pickle.load(infile)
    sys.stdout.write('Loaded dates:\n')
    for date in loaded_dates:
        sys.stdout.write(date.toMDYString() + '\n')
