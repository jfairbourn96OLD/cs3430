#!/usr/bin/python3

#######################################
# module: pickle_trial.py
# author: vladimir kulyukin
# description: pickle in Py3
# to run:
# $python pickle_trial.py dump.pck
#######################################

import pickle
import sys

with open(sys.argv[1], 'wb') as outfile:
    lst = ['a', 'b', [1, 2, 3], 'c', 'd']
    pickle.dump(lst, outfile)
    print('Dumped ' + str(lst))

with open(sys.argv[1], 'rb') as infile:
    lst = None
    lst = pickle.load(infile)
    print('Loaded ' + str(lst))
