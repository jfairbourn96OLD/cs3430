#!/usr/bin/python3

##################################################
# module: inheritance_01.py
# author: vladimir kulyukin
# description: inherting a method in a subclass
##################################################

class A:
    def hello(self):
        print('Hi, I am A!')

class B(A):
    pass




