#!/usr/bin/python3

##############################################
# module: inheritance.py
# author: vladimir kulyukin
# description: overriding hello() method in
# subclass
##############################################

class A:
    def hello(self):
        print('Hi, I am A!')

## overrding the method hello:
class B(A):
    def hello(self):
        print('Hi, I am B!')
        


