#!/usr/bin/python3

########################################
## module: multinh2.py
## author: vladimir kulyukin
## description: multiple inheritance
## in Py3.
########################################

class A(object):
    def __init__(self):
        self.x = 'A\'s x'
        print('A()')

    def f(self):
        print('A\'s f')

class B(object):
    def __init__(self):
        self.x = 'B\'s x'
        print('B()')

    def f(self):
        print('B\'s f')

class CAB(A, B):
    def __init__(self):
        super(CAB, self).__init__()
        print('CAB()')
        
class CBA(B, A):
    def __init__(self):
        super(CBA, self).__init__()
        print('CBA()')
        
