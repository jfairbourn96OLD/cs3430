#!/usr/bin/python3

#########################################
# module: cpickle_trial.py
# author: vladimir kulyukin
# description: cPickle in Py3
# to run:
# $python cpickle_trial.py cdump.pck
# Note thta in Py3 cPickle changed from
# cPickle to _pickle.
#########################################

import _pickle as cPickle
import sys

with open(sys.argv[1], 'wb') as outfile:
    lst = ['a', 'b', [1, 2, 3], 'c', 'd']
    cPickle.dump(lst, outfile)
    print('Dumped ' + str(lst))

with open(sys.argv[1], 'rb') as infile:
    lst = None
    lst = cPickle.load(infile)
    print('Loaded ' + str(lst))
