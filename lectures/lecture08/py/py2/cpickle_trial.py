#!/usr/bin/python

#########################################
# module: cpickle_trial.py
# author: vladimir kulyukin
# description: cPickle in Py2
# to run:
# $python cpickle_trial.py dump.pck
#########################################

import cPickle as pickle
import sys

with open(sys.argv[1], 'wb') as outfile:
    lst = ['a', 'b', [1, 2, 3], 'c', 'd']
    pickle.dump(lst, outfile)
    print('Dumped ' + str(lst))

with open(sys.argv[1], 'rb') as infile:
    lst = None
    lst = pickle.load(infile)
    print('Loaded ' + str(lst))
