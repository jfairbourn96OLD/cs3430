#!/usr/bin/python

__metaclass__ = type

###########################
# module: birds.py
# author: vladimir kulyukin
###########################

class A:
    def __init__(self):
        print('A.__init__()')
        self.x = 10
    def hello(self): print('A')

class B(A):
    def __init__(self):
        print('B.__init__()')
        super(B, self).__init__()
        self.y = 15
    def hello(self): print('B')

class C(A):
    def __init__(self):
        print('C.__init__()')
        super(C, self).__init__()
        self.z = 20
    def hello(self): print('C')

class D(B, C):
    def __init__(self):
        print('D.__init__()')
        super(D, self).__init__()
        self.w = 25
    def hello(self): print('D')

        

        


