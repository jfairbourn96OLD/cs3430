#!/usr/bin/python

__metaclass__ = type

###########################
# module: birds3.py
# author: vladimir kulyukin
# description: using __metaclass__ = type
# to use new-style classes in Py2
###########################

class Bird:
    def __init__(self):
        self.hungry = True

    def eat(self):
        if self.hungry:
            print('Aaaah...')
            self.hungry = False
        else:
            print('No, thanks!')

class SongBird(Bird):
    def __init__(self):
        super(SongBird, self).__init__()
        self.sound = 'Squawk!'
        
    def sing(self):
        print(self.sound)


        


