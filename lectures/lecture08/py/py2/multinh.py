#!/usr/bin/python

######################################
## module: multinh.py
## author: vladimir kulyukin
## description: multiple inheritance
## in Py2.
######################################

class A:
    def __init__(self):
        self.x = 'A\'s x'
        print('A()')

    def f(self):
        print('A\'s f')


class B:
    def __init__(self):
        self.x = 'B\'s x'
        print('B()')

    def f(self):
        print('B\'s f')


class CAB(A, B):
    def __init__(self):
        print('CAB()')

        
class CBA(B, A):
    def __init__(self):
        print('CBA()')
        
