#!/usr/bin/python

# bugs to vladimir dot kulyukin at usu dot edu

class Toop:
    """
    Toop is a class that constructs 1-, 2-, and 3-tuples.
    """

    def __init__(self, *args, **kwargs):
        if len(args) == 0 and len(kwargs) == 0:
            raise Exception('Not enough arguments')
        elif len(args) > 0 and len(kwargs) > 0:
            raise Exception('Cannot use both args and kwargs')
        elif len(args) > 3 or len(kwargs) > 3:
            raise Exception('Too many arguments')
        elif len(args) > 0:
            self.data = tuple(i for i in args)
        elif len(kwargs) > 0:
            first, second, third = None, None, None
            if kwargs.__contains__('first'):
                first = kwargs['first']
            if kwargs.__contains__('second'):
                second = kwargs['second']
            if kwargs.__contains__('third'):
                third = kwargs['third']
            self.data = (first, second, third)

    

