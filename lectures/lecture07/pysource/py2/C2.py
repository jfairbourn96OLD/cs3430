#!/usr/bin/python

class C2:
    """Example Class C2"""
    x = 12

    def __init__(self):
        self.data = (1, 2)

    def sayHi(self):
        print 'Hi, I am a Py C2 object!'

