#!/usr/bin/python

class C3:
    """Example Class C3"""

    def sayHi(self):
        print 'Hi, I am a Py C3 object!'

    def add2(self, x, y):
        return x+y

    def mult3(self, x, y, z):
        return x*y*z

