#!/usr/bin/python3

# bugs to vladimir dot kulyukin at usu dot edu

from Date import Date

d1 = Date(m=9, d=25, y=2016)
d2 = Date(m=9, d=26, y=2016)
d3 = Date(m=9, d=27, y=2016)

dates = [d1, d2, d3]
mdy_strings = [d.toMDYString() for d in dates]
ymd_strings = [d.toYMDString() for d in dates]
sorted_by_day = sorted(dates, key=lambda d: d.getDay())
mdy_sorted_by_day = [d.toMDYString() for d in sorted_by_day]

print(repr(dates))
print(repr(mdy_strings))
print(repr(ymd_strings))
print(repr(sorted_by_day))
print(repr(mdy_sorted_by_day))


    
