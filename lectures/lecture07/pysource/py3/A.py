#!/usr/bin/python3

class A:
    ## This is A.x class variable
    x = 12
    def g(self): return 'Hello from A instance'
    ## Change the value of A.x class variable
    def setClassX(self, xval):
       A.x = xval
    ## Change the value of self.x instance
    ## variable
    def setInstanceX(self, xval):
        self.x = xval
