#!/usr/bin/python3

################################
## CS 3430: S18
## using Python regular expressions to find and
## retrieve the components of an email address: 1)
## user name; 2) host name; 3) host extension.
## author: vladimir kulyukin
################################

import re

TXT_01 = '\n\
John   Balbiro	A1001	john.balbiro@usu.edu\n\
Alice  Nelson	A0011	alice.nelson@workflow.net\n\
Jacob  Roberts	A1100	j.s.roberts@gmail.com\n\
'

## we do not want to extract com|net|org|edu as a separate
## group, so we add ?: after the left parenthesis. this set of
## parentheses will not count as a group result
email_pat_with_1_group = r'([\w\.-]+@[\w\.-]+\.(?:com|net|org|edu))'

## group 1 - user name @ host name; group 2 extension
email_pat_with_2_groups = r'([\w\.-]+@[\w\.-]+)\.(com|net|org|edu)'

## group 2 - user name; group 2 - host name; group 3 - extension
email_pat_with_3_groups = r'([\w\.-]+)@([\w\.-]+)\.(com|net|org)'

email_matches_01 = re.findall(email_pat_with_1_group, TXT_01)
email_matches_02 = re.findall(email_pat_with_2_groups, TXT_01)
email_matches_03 = re.findall(email_pat_with_3_groups, TXT_01)

if __name__ == '__main__':
    print(email_matches_01)
    print(email_matches_02)
    print(email_matches_03)




