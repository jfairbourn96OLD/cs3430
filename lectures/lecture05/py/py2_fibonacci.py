#!/usr/bin/python

def fibrec(n):
    if n < 0: raise Exception('fibrec(n), n < 0')
    if n == 0 or n == 1:
        return n
    else:
        return fibrec(n-2) + fibrec(n-1)

def fibiter(n):
    if n < 0: raise Exception('fibiter(n), n < 0')
    if n == 0 or n == 1:
        return n
    prev_fib, curr_fib = 0, 1
    while n > 1:
        temp = curr_fib
        curr_fib = prev_fib + curr_fib
        prev_fib = temp
        n -= 1
    return curr_fib

