#!/usr/bin/python3

import sys

for argn, arg in zip(range(len(sys.argv)), sys.argv):
    print(argn, ' --> ', arg)
