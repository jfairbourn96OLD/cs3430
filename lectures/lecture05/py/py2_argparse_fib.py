#!/usr/bin/python

###############################################
# module: py2_argparse_fib.py
# bugs to vladimir dot kulyukin at usu dot edu
###############################################

import argparse
from datetime import datetime
from py2_fibonacci import *

ap = argparse.ArgumentParser()
ap.add_argument('-m', '--method', help='rec/iter', default='iter')
ap.add_argument('-n', '--n', help='nth Fibonacci to compute',
                type=int, default=0)
arg_table = vars(ap.parse_args()) ## place arguments into a dictionary

def time_fib():
    fibfun = None
    if arg_table['method'] == 'iter':
        fibfun = fibiter
    elif arg_table['method'] == 'rec':
        fibfun = fibrec
    else:
        raise Exception('Unknown method: ' + arg_table['method'])
    start = datetime.now()
    nth_fib = fibfun(arg_table['n'])
    end = datetime.now()
    print('%d-th Fibonacci: %d' % (arg_table['n'], nth_fib))
    print('time: %s' % str(end-start))

time_fib()
