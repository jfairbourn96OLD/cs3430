#!/usr/bin/python3

###############################################
# module: py3_process_lines.py
# bugs to vladimir dot kulyukin at usu dot edu
###############################################

import sys
from fibonacci import fibiter

def process_lines(file_path, fun):
    with open(file_path, 'r') as infile:
        return [fun(line) for line in infile]

## to run: $python py3_process_lines.py numbers.txt
if __name__ == '__main__':
    for rslt in process_lines(sys.argv[1], lambda x: int(x)+10):
        print(rslt)
    for rslt in process_lines(sys.argv[1], lambda x: fibiter(int(x))):
        print(rslt)
