#!/usr/bin/python3

###############################################
# module: py3_process_student_file.py
# Apply a regular expression to each line
# bugs to vladimir dot kulyukin at usu dot edu
###############################################

import sys
import re

# 1. define regexes
# ?: is a non-capturing version of a group - it will match com or
# net or org or edu but you will not be able to retrieve it via
# back reference.
a_num_and_email   = r'.*(A\d+)\s*([\w\.-]+@[\w\.-]+\.(?:com|net|org|edu)).*'
first_name_last_name_a_num = r'(\w+)\s*(\w+)\s*(A\d+).*'

def make_info_extractor(pat):
    def info_extractor(line):
        match = re.match(pat, line)
        if match is not None:
            return [match.group(i) for i in range(1, len(match.groups())+1)]
    return info_extractor

# 2. process file line by line and apply fun to each line
def process_lines(file_path, fun):
    with open(file_path, 'r') as infile:
        return [fun(line) for line in infile]

# 3. top level function
def main(file_path):
    print('-----------')
    for groups in process_lines(file_path, make_info_extractor(a_num_and_email)):
        print('\t'.join(groups))
    print('-----------')
    for groups in process_lines(file_path, make_info_extractor(first_name_last_name_a_num)):
        print('\t'.join(groups))
    print('-----------')

if __name__ == '__main__':
    main(sys.argv[1])

