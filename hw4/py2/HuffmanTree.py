#!/usr/bin/python

######################################
## module: HuffmanTreeNode.py
## Justin Fairbourn
## A01842387
######################################

from HuffmanTreeNode import HuffmanTreeNode
    
class HuffmanTree(object):
    def __init__(self, root=None):
        self.__root = root

    def getRoot(self):
        return self.__root

    '''
    After first making sure the symbol is valid (in the tree), we call the helper method 
        nextNodeEncode() that will handle the recursion.
    '''
    def encodeSymbol(self, s):
        if not s in self.__root.getSymbols():
            raise Exception('Unknown symbol: ' + s)
        else:
            return HuffmanTree.nextNodeEncode(self.__root, s)

    '''
    This method traverses through the Huffman tree and returns the encoded symbol.
    This method is static because it considers only one arbitrary node at a time.
    The recursion works as follows:
        - Base Case: If len(self.getsymbols) is 1, then you've arrived at a leaf and you
            are finished encoding this symbol. return out of the recursion
        - Recursion: If the symbol in question is in the set of symbols for the leftChild of
            the current node, then call this method on the next node while adding a 0 to the
            encoding. Otherwise, call this method on the rightChild and add a 1 to the
            encoding.
    Note: Because of the base case and the structure of Huffman Trees, all nodes this method
        receives should have two Non-null children, so no other assertions need to be made.
    '''
    @staticmethod
    def nextNodeEncode(node, s):
        if len(node.getSymbols()) == 1:
            return ''
        else:
            leftChild = node.getLeftChild()
            rightChild = node.getRightChild()
            if s in leftChild.getSymbols():
                return '0' + HuffmanTree.nextNodeEncode(leftChild, s)
            else:
                return '1' + HuffmanTree.nextNodeEncode(rightChild, s)

    '''
    This method simply initializes an empty string for the result, and then encodes each 
        character in that string and appends it to the result.
    '''
    def encodeText(self, txt):
        encoded_string = ''
        for symbol in txt:
            encoded_string += self.encodeSymbol(symbol)
        return encoded_string
 
    '''
    This method turns the binary string passed to it into a list so that values can be popped
        off. While there's still values in this list it will traverse through the tree and add
        characters it finds using the static method HuffmanTree.decodeSymbol()
    '''
    def decode(self, bin_string):
        bin_list = [bit for bit in bin_string]
        decoded_string = ''
        while(len(bin_list) > 0):
            decoded_string += HuffmanTree.decodeSymbol(self.getRoot(), bin_list)
        return decoded_string

    '''
    This method recursively traverses through the tree 'eating up' bits in the list and 
        taking steps to the left or right depending on what bit it eats.
    This method is static because it always does the same thing with each arbitary node it
        is passed.
    The recursion works like this:
        - Base Case: If the number of symbols the current node has is one, then you've reached
            a leaf and can return the corresponding character, exiting the recursion.
        - Recursion: If the next bit tells you to go left, call this function again passing the
            left child of the current node. Otherwise, go right by calling this function and
            passing the right child of the current node.
    Note: As with HuffmanTree.nextNodeEncode(), this requires no other assertions because every
        non-leaf node is guaranteed to have two children, so we can always call either 
        node.getLeftChild() or node.getRightChild() without worrying about NoneType calls.
    '''
    @staticmethod
    def decodeSymbol(node, bin_list):
        if len(node.getSymbols()) == 1:
            return max(node.getSymbols())[0]
        else:
            if bin_list.pop(0) is '0':
                return HuffmanTree.decodeSymbol(node.getLeftChild(), bin_list)
            else:
                return HuffmanTree.decodeSymbol(node.getRightChild(), bin_list)
    
    @staticmethod
    def mergeTwoNodes(htn1, htn2):
        #print 'Merging', str(htn1), str(htn2)
        symbols = set(htn1.getSymbols())
        for i in htn2.getSymbols():
            symbols.add(i)
        n = HuffmanTreeNode(symbols=symbols, weight=htn1.getWeight() + htn2.getWeight())
        n.setLeftChild(htn1)
        n.setRightChild(htn2)
        return n

    @staticmethod
    def findTwoLowestWeightNodes(list_of_nodes): 
        min_node_1 = min(list_of_nodes, key = lambda x: x.getWeight())
        list_of_nodes.remove(min_node_1)
        min_node_2 = min(list_of_nodes, key = lambda x: x.getWeight())
        list_of_nodes.remove(min_node_2)
        return(min_node_1, min_node_2)

    @staticmethod
    def displayListOfNodes(list_of_nodes):
        for n in list_of_nodes:
            print str(n)

    '''
    My implementation first finds the two 'lightest' nodes and removes them from the list,
    merges them, adds them to the list, and then calls itself with the new 'list_of_nodes' object.
    '''
    @staticmethod
    def fromListOfHuffmanTreeNodes(list_of_nodes):
        if len(list_of_nodes) == 0:
            raise Exception('Cannot construct from empty list of nodes')
        elif len(list_of_nodes) == 1:
            return HuffmanTree(root=list_of_nodes[0])
        else:
            min_nodes = HuffmanTree.findTwoLowestWeightNodes(list_of_nodes)
            list_of_nodes.append(HuffmanTree.mergeTwoNodes(min_nodes[0], min_nodes[1]))
            return HuffmanTree.fromListOfHuffmanTreeNodes(list_of_nodes)    

    @staticmethod
    def freqMapToListOfHuffmanTreeNodes(freq_map):
        return [HuffmanTreeNode(symbols=set([item[0]]), weight=item[1]) for item in freq_map.items()] 


    
