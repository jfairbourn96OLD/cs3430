#!/usr/bin/python

#################################
# module: encode_dna_strings.py
# YOUR NAME
# YOUR A-NUMBER
#################################

import re
import sys
import os
import fnmatch

from HuffmanTree import HuffmanTree
from HuffmanTreeNode import HuffmanTreeNode
from BinHuffmanTree import BinHuffmanTree
from CharFreqMap import CharFreqMap

'''
First, this method creates a list of HuffmanNodes, one for each nucleotide that we expect
  to see in a DNA sequence: A, G, T, and C. From these nodes, a regular HuffmanTree is 
  created (dna_ht), and from that HuffmanTree a BinHuffmanTree is created (dna_bht).
Then, sing the same generator code I created from HW3, I get a generator that spits out 
  tuples of the form: ('filepath', 'dna_string').
Finally, each HuffmanTree gets parsed to the BinHuffmanTree for encoding.
Note: While I was creating and debugging this, I came across multiple 'Unknown Symbol
  errors across many different files, and in each case the character in question was 'N'.
  Rather than adding 'N' as a Huffman Node, I just did a try/except for each encoding. I
  read through the assignment description and could not find anything that 
'''
def encode_dna_strings(fnpat, rootdir, encdir):
    dna_nodes = [HuffmanTreeNode(symbols=set([nucleotide[0]]), weight=nucleotide[1]) \
        for nucleotide in [('A', 1), ('G', 1), ('T', 1), ('C', 1)]]
    dna_ht = HuffmanTree.fromListOfHuffmanTreeNodes(dna_nodes)
    dna_bht = BinHuffmanTree(root=dna_ht.getRoot())
    file_generator = generate_file_names(fnpat, rootdir)
    input_generator = generate_input_streams(file_generator)
    dna_strings = [dna_string for dna_string in generate_dna_strings(input_generator)]
    for dna_string in dna_strings:
        dna_inpath = os.path.split(dna_string[0])[1]
        print 'encoding ' + encdir + dna_inpath
        try:
            dna_bht.encodeTextToFile(dna_string[1], encdir + dna_inpath)
        except Exception as exc:
            print exc.args[0] + ' found in ' + dna_inpath,
            print 'Skipping encoding of this file'    

'''My file_name generator from HW3'''
def generate_file_names(fnpat, rootdir):
    for path, dir_names, file_names in os.walk(rootdir):
        for file_name in file_names:
            if fnmatch.fnmatch(file_name, fnpat):
                yield os.path.join(path, file_name)

'''My input_stream generator from HW3'''
def generate_input_streams(gen_filenames):
    for file_name in gen_filenames:
        with open(file_name, 'r') as infile:
            yield (file_name, infile)

'''My dna_string generator from HW3'''
def generate_dna_strings(gen_instreams):
    for instream in gen_instreams:
        dna_string = ''
        for line in instream[1].read():
            dna_string += line
        yield (instream[0], re.sub('\n', '', dna_string))

if __name__ == '__main__':
    encode_dna_strings(sys.argv[1], sys.argv[2], sys.argv[3])














