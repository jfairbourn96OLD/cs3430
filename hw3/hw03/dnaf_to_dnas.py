#######################
# dnaf_to_dnas.py
# Justin Fairbourn
# A01842387
#######################

'''
This scipt reads a multiline string from STDIN, converts it to a single line string,
and writes it to STDOUT, as per the assignment description, item 3.
'''

import sys
import re

#initialize the string we'll add the data to
dna_string = ''

#append each line to the string from the pipe
for line in [line for line in sys.stdin.readlines()]:
    dna_string += line

#delete the \n characters
dna_string = re.sub('\n', '', dna_string)

#print it to the console
sys.stdout.write(dna_string)

#flush because the assignment description says to
sys.stdout.flush()
