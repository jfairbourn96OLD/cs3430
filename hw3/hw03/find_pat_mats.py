#######################
# find_pat_mats.py
# Justin Fairbourn
# A01842387
#######################

'''
(From the assignment description)
This script takes a regular expression specified by the user as the first argument on the command line,
finds all matches to this regular expression in a single line string from STDIN, and displays each match
on a new line as a tab-separated 3-tuple where the first element is the matched substring, the second element
is the substring's span's start index, and the third element is the substring's span's end index.
'''

import re
import sys

#use list comprehension to create the list of 3-tuples with from the matches in the pipe information
matches = [(match.group(), match.span()[0], match.span()[1]) for match in re.finditer(sys.argv[1], sys.stdin.readline())]

#write it in the way specified
for match in matches:
    sys.stdout.write(match[0] + '\t' + str(match[1]) + '\t' + str(match[2]) + '\n')


