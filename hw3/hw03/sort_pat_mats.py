#######################
# sort_pat_mats.py
# Justin Fairbourn
# A01842387
#######################

'''
(From the assignment description)
This script takes from the user a string argument in sys.argv[1] equal to either "asc" or "dsc", reads all tab-separated 3 tuples from STDN, sorts them out by span length ascendingly if sys.argv[1] is "asc" or descendingly if sys.argv[1] is "dsc", and prints the sorted 3-tuples to STDOUT so that each 3-tuple printed tab-separated on a single line.
'''

import sys
import re

#First read in the command (either "asc" or "dsc").
dsc = None
if sys.argv[1] ==  'asc':
    dsc = False
elif sys.argv[1] == 'dsc':
    dsc = True

#Now read in each of the strings from the previous "find_pat_mats.py" script call.
lst = [line for line in sys.stdin.readlines()]

#Change this string into usable data.
data_lst = [] 
for line in lst:
    match = re.search(r'(.*)\t(\d+)\t(\d+)', line)
    data_lst.append((match.group(1), match.group(2), match.group(3)))

#Now sort the data, and display!
data_lst.sort(key = lambda x: int(x[2]) - int(x[1]), reverse = dsc) 
for line in data_lst:
    sys.stdout.write(line[0] + '\t' + line[1] + '\t' + line[2] + '\n')
