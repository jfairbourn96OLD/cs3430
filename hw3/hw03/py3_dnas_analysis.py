#!/usr/bin/python3

#################################
# module: py3_dnas_analysis.py
# Justin Fairbourn
# A01842387
#################################

'''
Minimal documentation here since I essentially copied everything line-for-line from my solutions to Py2. There were no version errors that I could find.
'''

import re
import sys
import os
import fnmatch
import math

## --------- GENERATE_FILE_NAMES

def generate_file_names(fnpat, rootdir):
    for path, dir_names, file_names in os.walk(rootdir):
        for file_name in file_names:
            if fnmatch.fnmatch(file_name, fnpat):
                yield os.path.join(path, file_name)

def unit_test_01(fnpat, rootdir):
    for fn in generate_file_names(fnpat, rootdir):
        sys.stdout.write(fn + '\n')
    sys.stdout.flush()
        
## ----------- GENERATE_INPUT_STREAMS & GENERATE_LINES

def generate_input_streams(gen_filenames):
    for file_name in gen_filenames:
        with open(file_name, 'r') as infile:
            yield (file_name, infile)

def generate_dna_strings(gen_instreams):
    for instream in gen_instreams:
        dna_string = ''
        for line in instream[1].read():
            dna_string += line
        yield (instream[0], re.sub('\n', '', dna_string))

def unit_test_02(fnpat, rootdir):
    dna_fns  = generate_file_names(fnpat, rootdir)
    dna_ins  = generate_input_streams(dna_fns)
    dna_strs = generate_dna_strings(dna_ins)
    for fn, ds in dna_strs:
        sys.stdout.write(fn + '\t' + ds + '\t' + '\n')
    sys.stdout.flush()

def generate_pat_matches(pat, gen_dna_strings):
    for dna_string in gen_dna_strings:
        for match in re.finditer(pat, dna_string[1]):
            yield (dna_string[0], (match.group(), match.span()[0], match.span()[1])) 

def unit_test_03(fnpat, repat, rootdir):
    dna_fns  = generate_file_names(fnpat, rootdir)
    dna_ins  = generate_input_streams(dna_fns)
    dna_strs = generate_dna_strings(dna_ins)
    pat_mats = generate_pat_matches(repat, dna_strs)
    for fn, pm in pat_mats:
        sys.stdout.write(fn + '\t' + str(pm) + '\n')
    sys.stdout.flush()

def find_max_pat_match(fnpat, repat, rootdir):
    dna_fns  = generate_file_names(fnpat, rootdir)
    dna_ins  = generate_input_streams(dna_fns)
    dna_strs = generate_dna_strings(dna_ins)
    pat_mats = generate_pat_matches(repat, dna_strs)
    return max(pat_mats, key=lambda x: x[1][2] - x[1][1])

def find_min_pat_match(fnpat, repat, rootdir):
    dna_fns  = generate_file_names(fnpat, rootdir)
    dna_ins  = generate_input_streams(dna_fns)
    dna_strs = generate_dna_strings(dna_ins)
    pat_mats = generate_pat_matches(repat, dna_strs)
    return min(pat_mats, key=lambda x: x[1][2] - x[1][1])

def unit_test_04(fnpat, repat, rootdir):
    max_match = find_max_pat_match(fnpat, repat, rootdir)
    min_match = find_min_pat_match(fnpat, repat, rootdir)
    sys.stdout.write(repr(max_match)+'\n')
    sys.stdout.write(repr(min_match)+'\n')
  
if __name__ == '__main__':
#    unit_test_01(sys.argv[1], sys.argv[2])
#    unit_test_02(sys.argv[1], sys.argv[2])
#    unit_test_03(sys.argv[1], sys.argv[2], sys.argv[3])
#    unit_test_04(sys.argv[1], sys.argv[2], sys.argv[3])














