#!/usr/bin/python

#########################################
## CS 3430: S2018: HW01: Euclid Numbers
## Your Name
## Your A-Number
#########################################

import math

def is_prime(n):
    '''is_prime(n) ---> True if n is prime; False otherwise.'''
    # your code here
    pass

def next_prime_after(p):
    '''computes next prime after prime p; if p is not prime, returns None.'''
    if not is_prime(p): return None
    ## your code here
    pass

def euclid_number(i):
    '''euclid_number(i) --> i-th Euclid number.'''
    if i < 0: return None
   ## your code here
    pass

def compute_first_n_eucs(n):
    '''returns a list of the first n euclid numbers.'''
    eucs = []
    ## your code here
    return eucs

def prime_factors_of(n):
    '''returns a list of prime factors of n if n > 1 and [] otherwise.'''
    if n < 2: return []
    factors = []
    ## your code here
    return factors

def tabulate_euc_factors(n):
    '''returns a list of 3-tuples (i, euc, factors).'''
    euc_factors = []
    ## your code here
    return euc_factors

def pair(x, y):
    '''returns z such that <x, y> = z.'''
    ## your code
    pass

def find_pair_of(z):
    '''returns the tuple (x, y) such that <x, y> = z.''' 
    ## your code
    pass

def left_of(z):
    '''returns x such that <x, y> = z.'''
    ## your code
    pass

def right_of(z):
    '''returns y such that <x, y> = z.'''
    ## your code
    pass






                     

    

