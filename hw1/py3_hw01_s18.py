#!/usr/bin/python3

#########################################
## CS 3430: S2018: HW01: Euclid Numbers
## Justin Fairbourn
## A01842387
#########################################

import math


def is_prime(n):
    '''is_prime(n) ---> True if n is prime; False otherwise.'''
    # your code here
    nonprimes = list()
    primes = []
    for x in range(2, n):
        if x not in nonprimes:
            primes.append(x)
            for y in range(x**2, n, x):
                nonprimes.append(y)

    for x in primes:
        if n % x == 0:
            return False
    return True


def next_prime_after(p):
    '''computes next prime after prime p; if p is not prime, returns None.'''
    if not is_prime(p): return None
    ## your code here
    if not is_prime(p):
        return None
    if p == 2:
        return 3
    check = p + 2
    while True:
        if is_prime(check):
            return check
        check += 2


def euclid_number(i):
    '''euclid_number(i) --> i-th Euclid number.'''
    if i < 0: return None
    ## your code here
    if i < 0:
        return None
    j = 0
    prime_j = 2
    euclid = prime_j
    while j < i:
        prime_j = next_prime_after(prime_j)
        euclid *= prime_j
        j += 1
    return euclid + 1


def compute_first_n_eucs(n):
    '''returns a list of the first n euclid numbers.'''
    ## your code here
    eucs = []
    ## your code here
    for i in range(0, n):
        eucs.append(euclid_number(i))
    return eucs


def prime_factors_of(n):
    '''returns a list of prime factors of n if n > 1 and [] otherwise.'''
    if n < 2: return []
    factors = []
    test_factor = 2
    n_factored = n
    while test_factor < n_factored and test_factor < n/2:
        if n_factored % test_factor == 0:
            factors.append(test_factor)
            n_factored /= test_factor
        else:
            test_factor = next_prime_after(test_factor)
    if n_factored != 1:
        factors.append(int(n_factored))
    return factors



def tabulate_euc_factors(n):
    '''returns a list of 3-tuples (i, euc, factors).'''
    euc_factors = []
    ## your code here
    euc_factors = []
    for i in range(0, n):
        euc_i = euclid_number(i)
        euc_factors.append((i, euc_i, prime_factors_of(euc_i)))
    return euc_factors



def pair(x, y):
    '''returns z such that <x, y> = z.'''
    ## your code
    return 2 ** x * (2 * y + 1) - 1


def find_pair_of(z):
    '''returns the tuple (x, y) such that <x, y> = z.'''
    for x in range(0, z):
        y = (.5) * (((z+1)/(2**x))-1)
        if y % 1 == 0 and y >= 0:
            return (x, int(y))


def left_of(z):
    '''returns x such that <x, y> = z.'''
    ## your code
    return find_pair_of(z)[0]


def right_of(z):
    '''returns y such that <x, y> = z.'''
    ## your code
    return find_pair_of(z)[1]







